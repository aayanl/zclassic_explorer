<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once("daemon.php");

$getinfo = getinfo();
$CMCdata = json_decode(CallAPI('get', 'https://api.coinmarketcap.com/v1/ticker/zclassic/'));

function CallAPI($method, $url, $data = false)    //http://stackoverflow.com/a/9802854
{
    $curl = curl_init();

    switch ($method)
    {
        case "POST":
            curl_setopt($curl, CURLOPT_POST, 1);

            if ($data)
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            break;
        case "PUT":
            curl_setopt($curl, CURLOPT_PUT, 1);
            break;
        default:
            if ($data)
                $url = sprintf("%s?%s", $url, http_build_query($data));
    }

    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

    $result = curl_exec($curl);

    curl_close($curl);

    return $result;
}

function humanSince($pastepochtime) {
    $humanTime = (int) date('i', time() - $pastepochtime);

    if ($humanTime == 0) {
      return 'Few seconds ago';
    } elseif ($humanTime == 1) {
        return $humanTime . ' minute ago';
    } else {
      return $humanTime . ' minutes ago';
    }
}

?>

<!doctype html>
<html lang="en">
   <head>
      <bas   href="/" />
      <meta charset="UTF-8">
      <title>Zclassic explorer</title>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <link rel="stylesheet" href="css/main.min.css">
   </head>
   <body style="background-color: #f8f8f8; color: black;">
      <nav class="container">
         <div data-ng-controller="HeaderController">
            <div class="navbar-header">
               <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse" data-ng-click="$root.isCollapsed = !$root.isCollapsed">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               </button>
               <a class="insight navbar-brand" href="."><img src="https://raw.githubusercontent.com/z-classic/z-classic.github.io/master/img/logo-light.png" alt="Zclassic" width="125" height="30"></img>
               </a>
            </div>
            <div class="navbar-collapse collapse" collapse="$root.isCollapsed">
               <ul class="nav navbar-nav">
                  <li data-ng-repeat="item in menu" ui-route="{{item.link}}" data-ng-class="{active: $uiRoute}">
                      <a href="blocks.php">Blocks</a>
                  </li>
                  <li><h5>THIS IS A WIP PORT TO PHP FROM NODEJS, USE <a href="http://aayanl.tech:3001/insight">THIS INSTEAD</a></h1></li>
               </ul>
               <ul class="nav navbar-nav navbar-right">
                  <li>
                     <div class="status" data-ng-controller="StatusController">
                        <strong>Conn</strong> <?php echo $getinfo['connections']; ?>
                        &middot;
                        <strong>Height</strong> <?php echo $getinfo['blocks']; ?>
                     </div>
                  </li>
                  <li>
                     <a><span class="glyphicon glyphicon-qrcode"></span> Scan</a>
                  </li>
                  <li class="dropdown" data-ng-controller="CurrencyController" data-ng-include="'views/includes/currency.html'"></li>
               </ul>
            </div>
         </div>
      </nav>

      <header>
         <section class="jumbotron background-image-holder fadeIn container-fluid" style='background-image: url("http://zclassic.org/img/bgecl5.jpg"); background-repeat: repeat;'>
            <div class="container">
               <div class="row" data-ng-controller="StatusController">
                  <div class="col-md-3 hidden-xs hidden-sm col-gray">
                     <center>
                        <h3>ZClassic Price</h3>
                     </center>
                     <p id="price" onload="start()" style="border-top: 1px solid #aaa; border-bottom: none;border-left: none;border-right: none;display: block;text-align: center; color: black;">
                      <?php
                        print("BTC: " . $CMCdata[0]->price_btc . "; USD: ". $CMCdata[0]->price_usd);
                      ?>
                    </p>
                  </div>
                  <div class="col-md-6 hidden-xs hidden-sm">
                     <center>
                        <a href="http://zclassic.org"><img src="http://imgh.us/zcl_1.png" style="width: 25%; height: auto;">
                        </a>
                     </center>
                  </div>
                  <div class="col-md-3 col-gray hidden-xs hidden-sm">
                     <center>
                        <h3>Difficulty</h3>
                     </center>
                     <p style="border-top: 1px solid #aaa; border-bottom: none;border-left: none;border-right: none;display: block;text-align: center; color: black;"><span><?php echo $getinfo['difficulty']; ?></span></p>
                  </div>
               </div>
               <div class="row visible-xs visible-sm">
                  <div class="visible-xs visible-sm col-xs-12 col-sm-12">
                     <a href="http://zclassic.org"><img src="http://imgh.us/zcl_1.png" style="width: 5%; height: auto;">
                     </a>
                  </div>
               </div>
               <br>
               <br>
               <span class="hidden-xs" style="color: black">
                 <form id="search-form" data-ng-controller="SearchController" role="search" data-ng-submit="search()">
                   <div class="form-group">
                     <input id="search" type="text" class="form-control" data-ng-model="q" data-ng-class="{'loading': loading}" placeholder="Search for block, transaction or address" data-ng-submit="search()" focus="true">
                    </div>
                 </form>
               </span>
            </div>
         </section>
      </header>

      <main class="container-fluid">
        <section class="container">
          <br>
          <br>
          <!--<h2>This explorer will be down for maintenance at 3/4/17 from 12am est to 8 am est</h2>-->
          <br>
          <br>
          <div class="container">
            <div class="row" id="home">
              <div class="col-md-8 col-xs-12">
                <h1 translate>Latest Blocks</h1>
                <table class="table table-hover table-striped" style="table-layout: fixed;">
                  <thead>
                    <tr>
                      <th translate>Height</th>
                      <th translate>Age</th>
                      <th class="text-right"><span class="ellipsis" translate>Transactions</span></th>
                      <th class="text-right hidden-xs"><span class="ellipsis" translate>Mined by</span></th>
                      <th class="text-right" translate>Size</th>
                    </tr>
                  </thead>
                  <tbody>
                      <!--
                      <td>
                        <a href="block/{{b.hash}}">{{b.height}}</a>
                      </td>
                      <td><span class="ellipsis">{{humanSince(b.time)}}</span></td>
                      <td class="text-right">{{b.txlength}}</td>
                      <td class="text-right hidden-xs">
                        <a data-ng-show="b.poolInfo" href="{{b.poolInfo.url}}" target="_blank" title="{{b.poolInfo.poolName}}">{{b.poolInfo.poolName}}</a>
                      </td>
                      <td class="text-right">{{b.size}}</td>
                      -->
                      <?php
                         for ($i = $getinfo['blocks']; $i > $getinfo['blocks'] - 5; $i--) {
                           $block = getblock(getblockhash($i));
                           echo '<tr class="fader">';
                              echo '<td><a href="block/TODO">'.$i.'</a></td>';
                              echo '<td><span class="ellipsis">'.humanSince($block['time']).'</span></td>';
                              echo '<td class="text-right">'.count($block['tx']).'</td>';
                              echo '<td class="text-right">TODO ADD POOLS</td>';
                              echo '<td class="text-right">'.$block['size'].'</td>';
                            echo '</tr>';
                         }
                      ?>
                  </tbody>
                </table>
                <div class="btn-more">
                  <a class="btn btn-default" href="blocks" translate>See all blocks</a>
                </div><br>
                <br>
                <br>
                <h1 translate>Latest Transactions</h1>
                <table class="table table-hover table-striped" style="table-layout: fixed;">
                  <thead>
                    <tr>
                      <th>Hash</th>
                      <th class="text-right" translate>Value Out</th>
                    </tr>
                  </thead>
                  <tbody>
                    <!--<tr class="fader" data-ng-repeat='tx in txs'>
                      <td>
                        <a class="ellipsis" href="tx/{{tx.txid}}">{{tx.txid}}</a>
                      </td>
                      <td class="text-right"><span class="ellipsis">{{$root.currency.getConvertion(tx.valueOut)}}</span></td>
                    </tr>-->
                    <?php
                        $mempool = getrawmempool();
                        $i =  '314c37e36f66626790ac99437aede66b167426f86e823850efcff3732ee4643b';
                        //foreach ($mempool as $i) {
                          echo '<tr class="fader">';
                            echo '<td><a class="ellipsis" href="tx/' .$i. '">'.$i.'</a></td>';
                            echo '<td class="text-right"><span class="ellipsis">'.getrawtransaction($i).'</span></td>';
                          echo '</tr>';
                        //}
                    ?>
                  </tbody>
                </table><br>
                <div>
                  <h2>Known bugs</h2>
                  <p>Mining addresses(Suprnova and a couple others) that pay to a public key do not show up when you search their balance and transactions from them are unparsed.</p>
                </div>
              </div>
              <div class="col-md-4">
                <div>
                  <h1 translate>Network Info</h1>
                  <table class="table table-hover table-striped" style="table-layout: fixed;">
                    <thead>
                      <tr>
                        <th>Hashrate</th>
                        <th translate>Total coins</th>
                        <th class="text-right">Marketcap</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr class="fader">
                        <td id="hashrate"><?php echo getnetworkhashps() / 1e6 . ' mS/s'; ?></td>
                        <td id="totalCoins"><?php echo $getinfo['blocks'] * 12.5 . ' ZCL'; ?></td>
                        <td class="text-right"><?php echo '$'.$CMCdata[0]->market_cap_usd; ?></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <br>
                <div id="memoDiv">
                  <h1 translate>Memos</h1>
                  <p>Want to encode text into the blockchain? Send some ZCL and a memo to <a href='#' onclick="alert('zcgaBHzeqqKkDR8ivX38Qp7foMwuYLnGct6rbV9wo66r2xdk1BZoSHiEqczRurd9MUu2womGHcvhkWMMuqkSCKsviKtA1hs')"><i>zcgaBHzeqqKkDR...</i></a></p>
                </div>
                <p><i>&lt;3 to whoever sent me that 10 ZCL.</i></p><br>
              </div>
            </div>
          </div>
        </section>
      </main>
   </body>
</html>
